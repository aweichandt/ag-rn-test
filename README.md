
# Addison Global react native test

## Setup and run
Project made using node v7.3.0 and npm v5.3.0

Please follow up react-native [getting started](https://facebook.github.io/react-native/docs/getting-started) section and ensure you've set up all needed dependencies to run the project.


## Running on iOS or Android
Run the project on from command line like this: 
#### for iOS
```cmd
react-native run-ios
```
#### for Android
```cmd
react-native run-android
```

## Test
This project comes with jest test environment already set. try it out from command line:
```cmd
npm test
```
A coverage report can also be made by running
```cmd
npm run coverage
```

