import { createMiddleware } from '../../navigation/helpers/redux';

const middleware = createMiddleware();
export default middleware;
