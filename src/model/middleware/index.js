import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import navigation from './navigation';

const devMiddlewares = [
  logger,
];
let middlewares = [
  thunk,
  navigation,
];
/* global __DEV__ */
if (__DEV__) {
  middlewares = [...middlewares, ...devMiddlewares];
}
export default applyMiddleware(...middlewares);
