import {
  BET, UNBET,
} from '../const/actionNames';
import fetchEvents from './fetchEvents';

export * from './navigation';

const baseEvent = type => (value, key) => ({ type, value, key });

export const betAction = baseEvent(BET);
export const unbetAction = baseEvent(UNBET);
export const fetchEventsAction = fetchEvents;
