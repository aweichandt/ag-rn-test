import configureStore from 'redux-mock-store';
import {
  openDrawerAction,
  closeDrawerAction,
} from '../navigation';

const mockStore = configureStore([]);
const initialState = {};

describe('navigation actions test suite', () => {
  it('returns expected open drawer action', () => {
    const store = mockStore(initialState);
    store.dispatch(openDrawerAction());
    expect(store.getActions()).toEqual([
      { type: 'Navigation/OPEN_DRAWER' },
    ]);
  });
  it('returns expected close drawer action', () => {
    const store = mockStore(initialState);
    store.dispatch(closeDrawerAction());
    expect(store.getActions()).toEqual([
      { type: 'Navigation/CLOSE_DRAWER' },
    ]);
  });
});
