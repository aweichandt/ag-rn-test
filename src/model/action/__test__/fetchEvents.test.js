import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import fetchEvents from '../fetchEvents';
import { FETCH, EVENTS_DATA } from '../../const/actionNames';
import { EVENTS } from '../../const/networkEvents';

const middlewares = [thunk]; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {};

const mockData = [
  {
    id: 'evt0',
    name: 'event0',
    markets: [
      {
        id: 'mkt0',
        name: 'market0',
        selections: [
          { id: 's0', name: 'sel0', price: 1.22 },
          { id: 's1', name: 'sel1', price: 2.11 },
        ],
      },
    ],
  },
];

describe('fetchEvents test suite', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });
  it('returns expected actions on query success', () => {
    const body = JSON.stringify(mockData);
    fetch.once(body);

    const store = mockStore(initialState);
    return store.dispatch(fetchEvents()).then(() => expect(store.getActions()).toEqual([
      { type: FETCH, key: EVENTS, value: true },
      { type: EVENTS_DATA, value: mockData, error: undefined },
      { type: FETCH, key: EVENTS, value: false },
    ]));
  });
  it('returns error on fetch response error', () => {
    const body = JSON.stringify(mockData);
    fetch.once(body, { status: 409 });

    const store = mockStore(initialState);
    return store.dispatch(fetchEvents()).then(() => expect(store.getActions()).toEqual([
      { type: FETCH, key: EVENTS, value: true },
      { type: EVENTS_DATA, value: undefined, error: expect.any(Error) },
      { type: FETCH, key: EVENTS, value: false },
    ]));
  });
  it('returns error on fetch request error', () => {
    const error = new Error('Network request failed');
    fetch.mockRejectOnce(error);

    const store = mockStore(initialState);
    return store.dispatch(fetchEvents()).then(() => expect(store.getActions()).toEqual([
      { type: FETCH, key: EVENTS, value: true },
      { type: EVENTS_DATA, value: undefined, error },
      { type: FETCH, key: EVENTS, value: false },
    ]));
  });
});
