import configureStore from 'redux-mock-store';
import {
  betAction,
  unbetAction,
} from '..';
import { BET, UNBET } from '../../const/actionNames';

const mockStore = configureStore([]);
const initialState = {};

describe('base actions test suite', () => {
  it('returns expected bet action', () => {
    const value = 'selectionId';
    const store = mockStore(initialState);
    store.dispatch(unbetAction(value));
    return expect(store.getActions()).toEqual([
      { type: UNBET, value },
    ]);
  });
  it('returns expected bet action', () => {
    const value = 'selectionId';
    const store = mockStore(initialState);
    store.dispatch(betAction(value));
    return expect(store.getActions()).toEqual([
      { type: BET, value },
    ]);
  });
});
