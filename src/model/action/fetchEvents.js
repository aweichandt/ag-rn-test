import {
  FETCH,
  EVENTS_DATA,
} from '../const/actionNames';
import { EVENTS } from '../const/networkEvents';

const endpoint = 'http://www.mocky.io/v2/59f08692310000b4130e9f71';

const handleResponse = (response) => {
  const { ok } = response;
  if (!ok) {
    throw new TypeError('Network Request Failed');
  }
  return response.json();
};

const fetchEvents = () => (dispatch) => {
  dispatch({ type: FETCH, key: EVENTS, value: true });
  return fetch(endpoint, { method: 'GET', credentials: 'include' })
    .then(handleResponse)
    .then(value => dispatch({ type: EVENTS_DATA, value }))
    .catch(error => dispatch({ type: EVENTS_DATA, error }))
    .then(() => dispatch({ type: FETCH, key: EVENTS, value: false }));
};

export default fetchEvents;
