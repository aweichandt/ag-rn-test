import { DrawerActions } from 'react-navigation';

export const openDrawerAction = DrawerActions.openDrawer;
export const closeDrawerAction = DrawerActions.closeDrawer;
