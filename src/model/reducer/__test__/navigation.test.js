import { DrawerActions } from 'react-navigation';
import reducer from '../navigation';

const initialState = {
  index: 0,
  isDrawerOpen: false,
  isTransitioning: false,
  openId: 0,
  closeId: 0,
  toggleId: 0,
  routes: [
    {
      index: 0,
      isTransitioning: false,
      key: 'MAIN',
      routeName: 'MAIN',
      routes: [
        {
          key: expect.any(String),
          routeName: 'MAIN/EVENTS',
        },
      ],
    },
  ],
};

describe('navigation reducer test suite', () => {
  it('resolves initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should open drawer', () => {
    const action = DrawerActions.openDrawer();
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      openId: 1,
    });
  });
  it('should navigate back from drawer', () => {
    const action = DrawerActions.closeDrawer();
    expect(reducer({ ...initialState, openId: 1 }, action)).toEqual({
      ...initialState,
      openId: 1,
      closeId: 1,
    });
  });
  it('should toggle drawer', () => {
    const action = DrawerActions.toggleDrawer();
    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      toggleId: 1,
    });
    expect(reducer({ ...initialState, toggleId: 1 }, action)).toEqual({
      ...initialState,
      toggleId: 2,
    });
  });
});
