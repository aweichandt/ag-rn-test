import reducer from '../selections';
import { EVENTS_DATA } from '../../const/actionNames';

const mockData = [
  {
    id: 'evt0',
    name: 'event0',
    markets: [
      {
        id: 'mkt0',
        name: 'market0',
        selections: [
          { id: 's0', name: 'sel0', price: 1.22 },
          { id: 's1', name: 'sel1', price: 2.11 },
        ],
      },
      {
        id: 'mkt1',
        name: 'market1',
        selections: [
          { id: 's3', name: 'sel3', price: 1.22 },
        ],
      },
    ],
  },
  {
    id: 'evt0',
    name: 'event0',
    markets: [],
  },
];
const addedEvt = {
  id: 'evt2',
  name: 'event2',
  markets: [
    {
      id: 'mkt2',
      name: 'market2',
      selections: [
        { id: 's4', name: 'sel4', price: 3 },
      ],
    },
  ],
};

const initialState = {};

const evtState = {
  s0: { id: 's0', name: 'sel0', price: 1.22 },
  s1: { id: 's1', name: 'sel1', price: 2.11 },
  s3: { id: 's3', name: 'sel3', price: 1.22 },
};

describe('selections reducer test suite', () => {
  it('resolves initialState', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('hshould handle success events data load', () => {
    const action = { type: EVENTS_DATA, value: mockData };
    expect(reducer(initialState, action)).toEqual(evtState);
  });
  it('should keep state on events data error', () => {
    const action = { type: EVENTS_DATA, error: 'NetworkError' };
    expect(reducer(initialState, action)).toEqual(initialState);
    expect(reducer(evtState, action)).toEqual(evtState);
  });
  it('should handle event data refresh', () => {
    const newState = {
      ...evtState,
      s4: { id: 's4', name: 'sel4', price: 3 },
    };
    const action = { type: EVENTS_DATA, value: mockData };
    const newAction = { type: EVENTS_DATA, value: [...mockData, addedEvt] };
    expect(reducer(evtState, newAction)).toEqual(newState);
    expect(reducer(newState, newAction)).toEqual(newState);
    expect(reducer(newState, action)).toEqual(evtState);
  });
});
