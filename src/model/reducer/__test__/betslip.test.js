import reducer from '../betslip';
import { BET, UNBET } from '../../const/actionNames';

const initialState = [];

const betState = ['s1'];
const bet2State = ['s2'];
const twoBetState = ['s1', 's2'];

describe('btslip reducer test suite', () => {
  it('resolves initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should add new bet on BET', () => {
    const action = { type: BET, value: 's1' };
    expect(reducer(initialState, action)).toEqual(betState);
    expect(reducer(bet2State, action)).toEqual(['s2', 's1']);
  });
  it('should keep same beton BET', () => {
    const action = { type: BET, value: 's1' };
    expect(reducer(betState, action)).toEqual(betState);
    expect(reducer(twoBetState, action)).toEqual(twoBetState);
  });
  it('should remove bet if exists on unbet', () => {
    const action = { type: UNBET, value: 's1' };
    expect(reducer(betState, action)).toEqual(initialState);
    expect(reducer(twoBetState, action)).toEqual(bet2State);
  });
  it('should return same state if unbet is missing', () => {
    const action = { type: UNBET, value: 's1' };
    expect(reducer(initialState, action)).toEqual(initialState);
    expect(reducer(bet2State, action)).toEqual(bet2State);
  });
  it('does not break for undefined values', () => {
    const betAction = { type: BET };
    expect(reducer(initialState, betAction)).toEqual(initialState);
    const unbetAction = { type: UNBET };
    expect(reducer(betState, unbetAction)).toEqual(betState);
  });
});
