import reducer from '../network';
import { FETCH } from '../../const/actionNames';

const initialState = {};

describe('network reducer test suite', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should flag network event', () => {
    const action = { type: FETCH, key: 'test', value: true };
    expect(reducer(initialState, action)).toEqual({
      test: true,
    });
  });
  it('should remove network event', () => {
    const action = { type: FETCH, key: 'test', value: false };
    expect(reducer({ test: true }, action)).toEqual(initialState);
  });
  it('should work with no keys', () => {
    const beginAction = { type: FETCH, key: 'test', value: false };
    expect(reducer(initialState, beginAction)).toEqual(initialState);
    const endAction = { type: FETCH, key: 'test', value: true };
    expect(reducer({ test: true }, endAction)).toEqual({
      test: true,
    });
  });
});
