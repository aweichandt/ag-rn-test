import reducer from '../events';
import { EVENTS_DATA } from '../../const/actionNames';

const mockData = [
  {
    id: 'evt0',
    name: 'event0',
    markets: [
      {
        id: 'mkt0',
        name: 'market0',
        selections: [
          { id: 's0', name: 'sel0', price: 1.22 },
          { id: 's1', name: 'sel1', price: 2.11 },
        ],
      },
      {
        id: 'mkt1',
        name: 'market1',
        selections: [
          { id: 's3', name: 'sel3', price: 1.22 },
        ],
      },
    ],
  },
  {
    id: 'evt1',
    name: 'event1',
    markets: [],
  },
];
const addedEvt = {
  id: 'evt2',
  name: 'event2',
  markets: [
    {
      id: 'mkt2',
      name: 'market2',
      selections: [
        { id: 's4', name: 'sel4', price: 3 },
      ],
    },
  ],
};

const initialState = {};

const evtState = {
  evt0: {
    id: 'evt0',
    name: 'event0',
    markets: ['mkt0', 'mkt1'],
  },
};

describe('events reducer test suite', () => {
  it('resolves initialState', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it('should handle success events data load', () => {
    const action = { type: EVENTS_DATA, value: mockData };
    expect(reducer(initialState, action)).toEqual(evtState);
  });
  it('should keep state on events data error', () => {
    const action = { type: EVENTS_DATA, error: 'NetworkError' };
    expect(reducer(initialState, action)).toEqual(initialState);
    expect(reducer(evtState, action)).toEqual(evtState);
  });
  it('should handle event data refresh', () => {
    const newState = {
      ...evtState,
      evt2: {
        ...addedEvt,
        markets: ['mkt2'],
      },
    };
    const action = { type: EVENTS_DATA, value: mockData };
    const newAction = { type: EVENTS_DATA, value: [...mockData, addedEvt] };
    expect(reducer(evtState, newAction)).toEqual(newState);
    expect(reducer(newState, newAction)).toEqual(newState);
    expect(reducer(newState, action)).toEqual(evtState);
  });
});
