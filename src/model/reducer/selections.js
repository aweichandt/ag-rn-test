import { EVENTS_DATA } from '../const/actionNames';

const mapSelections = (list = []) => list.reduce((acc, e) => ({ ...acc, [e.id]: e }), {});

const initialState = {};

const selections = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case EVENTS_DATA: {
      const { value } = action;
      if (value) {
        const allMarkets = value.reduce((acc, { markets: mks }) => [...acc, ...mks], []);
        const allSelections = allMarkets.reduce((sls, { selections: s }) => [...sls, ...s], []);
        return mapSelections(allSelections);
      }
      return state;
    }
    default:
      return state;
  }
};

export default selections;
