import { FETCH } from '../const/actionNames';

const initialState = {};

const network = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case FETCH: {
      const { key, value } = action;
      if (key) {
        return {
          ...state,
          [key]: value ? true : undefined,
        };
      }
      return state;
    }
    default:
      return state;
  }
};

export default network;
