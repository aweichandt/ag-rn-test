import { EVENTS_DATA } from '../const/actionNames';

const mapEvents = (list = []) => list.reduce((acc, { markets: mkts = [], ...e }) => {
  const markets = mkts.map(({ id }) => id);
  return markets.length > 0 ? { ...acc, [e.id]: { ...e, markets } } : acc;
}, {});

const initialState = {};

const events = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case EVENTS_DATA: {
      const { value } = action;
      if (value) {
        return mapEvents(value);
      }
      return state;
    }
    default:
      return state;
  }
};

export default events;
