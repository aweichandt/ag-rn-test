import { combineReducers } from 'redux';
import events from './events';
import markets from './markets';
import selections from './selections';
import betslip from './betslip';
import navigation from './navigation';
import network from './network';

const reducers = {
  events,
  markets,
  selections,
  betslip,
  navigation,
  network,
};

export default combineReducers(reducers);
