import { BET, UNBET } from '../const/actionNames';

const initialState = [];

const betslip = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case BET: {
      const { value } = action;
      if (!!value && !state.includes(value)) {
        return [...state, value];
      }
      return state;
    }
    case UNBET: {
      const { value } = action;
      return state.filter(s => s !== value);
    }
    default:
      return state;
  }
};

export default betslip;
