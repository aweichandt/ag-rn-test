import { EVENTS_DATA } from '../const/actionNames';

const mapMarkets = (list = []) => list.reduce((acc, { selections: sels, ...e }) => {
  const selections = sels.map(({ id }) => id);
  return { ...acc, [e.id]: { ...e, selections } };
}, {});

const initialState = {};

const markets = (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case EVENTS_DATA: {
      const { value } = action;
      if (value) {
        const allMarkets = value.reduce((acc, { markets: mks }) => [...acc, ...mks], []);
        return mapMarkets(allMarkets);
      }
      return state;
    }
    default:
      return state;
  }
};

export default markets;
