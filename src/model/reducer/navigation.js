import { createReducer } from '../../navigation/helpers/redux';
import AppRouter from '../../navigation/router';

const navigation = createReducer(AppRouter);

export default navigation;
