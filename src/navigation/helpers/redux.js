import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
// redux helpers
export const createNavigator = (navigator, listener = 'root') => reduxifyNavigator(navigator, listener);
export const createMiddleware = (listener = 'root') => createReactNavigationReduxMiddleware(listener, state => state.nav);
export const createReducer = navigator => createNavigationReducer(navigator);
