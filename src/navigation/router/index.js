import { createDrawerNavigator } from 'react-navigation';
import { MAIN } from '../../model/const/routeNames';
import MainRoute from './main';
import { DrawerBetslipScreen } from '../../container/screen';

const config = {
  [MAIN]: {
    screen: MainRoute,
  },
};
const drawerConfig = {
  drawerPosition: 'right',
  contentComponent: DrawerBetslipScreen,
};

export default createDrawerNavigator(config, drawerConfig);
