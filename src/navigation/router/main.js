import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { MAIN_EVENTS } from '../../model/const/routeNames';
import { MainEventsScreen } from '../../container/screen';
import DrawerNavigationButton from '../../container/navigation/DrawerNavigationButton';

const config = {
  [MAIN_EVENTS]: {
    screen: MainEventsScreen,
    navigationOptions: () => ({
      title: 'Main Events',
      headerRight: <DrawerNavigationButton />,
    }),
  },
};

export default createStackNavigator(config, { headerMode: 'float' });
