import React from 'react';
import {
  ViewPropTypes,
  StyleSheet,
} from 'react-native';
import { Button, Icon } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    marginLeft: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    height: 40,
  },
  icon: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
  },
});

const DrawerButton = ({ style, icon, ...props }) => (
  <Button
    iconRight
    transparent
    {...props}
    style={style}
  >
    <Icon style={styles.text} name={icon} />
  </Button>
);
DrawerButton.propTypes = {
  style: ViewPropTypes.style,
  icon: PropTypes.oneOf(['menu', 'close']),
};
DrawerButton.defaultProps = {
  style: styles.container,
  icon: 'menu',
};

export default DrawerButton;
