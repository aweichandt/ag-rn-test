import React from 'react';
import { shallow } from 'enzyme';

import Separator from '../Separator';

describe('Separator test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Separator />);
    expect(wrapper).toMatchSnapshot();
  });
});
