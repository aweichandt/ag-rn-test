import React from 'react';
import { shallow } from 'enzyme';

import DrawerButton from '../DrawerButton';

describe('DrawerButton test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<DrawerButton />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should support style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<DrawerButton style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports icon prop', () => {
    const icon = 'close';
    const wrapper = shallow(<DrawerButton icon={icon} />);
    expect(wrapper.find('Styled(Icon)').prop('name')).toEqual(icon);
  });
  it('supports onPress prop', () => {
    const onPress = jest.fn();
    const wrapper = shallow(<DrawerButton onPress={onPress} />);
    wrapper.simulate('press');
    expect(onPress).toBeCalled();
  });
});
