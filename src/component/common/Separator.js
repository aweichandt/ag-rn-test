import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
// import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '90%',
    height: 1,
    backgroundColor: 'lightgray',
  },
});

const Separator = () => (
  <View style={styles.container} />
);
Separator.propTypes = {
};
Separator.defaultProps = {
};

export default Separator;
