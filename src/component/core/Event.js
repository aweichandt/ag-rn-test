import React from 'react';
import {
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
import { Card, Text, CardItem } from 'native-base';
import PropTypes from 'prop-types';
import Market from './Market';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  title: {
    fontSize: 18,
  },
});

const Event = ({
  style, id, name, markets, MarketComponent,
}) => (
  <Card key={id} style={style}>
    <CardItem bordered header>
      <Text style={styles.title}>{name}</Text>
    </CardItem>
    {markets.map(k => (
      <MarketComponent id={k} key={k} />
    ))}
  </Card>
);
Event.propTypes = {
  style: ViewPropTypes.style,
  id: PropTypes.string,
  name: PropTypes.string,
  markets: PropTypes.arrayOf(PropTypes.string),
  MarketComponent: PropTypes.func,
};
Event.defaultProps = {
  style: styles.container,
  id: undefined,
  name: '',
  markets: [],
  MarketComponent: Market,
};

export default Event;
