import React from 'react';
import { shallow } from 'enzyme';

import NoBet from '../NoBet';

describe('NoBet test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<NoBet />);
    expect(wrapper).toMatchSnapshot();
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<NoBet style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
});
