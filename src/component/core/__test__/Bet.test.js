import React from 'react';
import { shallow } from 'enzyme';

import Bet from '../Bet';

describe('Bet test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Bet />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('Styled(Text)').at(0).prop('children')).toEqual('');
    expect(wrapper.find('Styled(Text)').at(1).prop('children')).toEqual(0);
    expect(wrapper.find('Styled(Text)').at(2).prop('children')).toEqual('Delete');
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<Bet style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports name prop', () => {
    const name = 'test';
    const wrapper = shallow(<Bet name={name} />);
    expect(wrapper.find('Styled(Text)').at(0).prop('children')).toEqual(name);
  });
  it('supports price prop', () => {
    const price = 22.31;
    const wrapper = shallow(<Bet price={price} />);
    expect(wrapper.find('Styled(Text)').at(1).prop('children')).toEqual(price);
  });
  it('supports action prop', () => {
    const action = 'test';
    const wrapper = shallow(<Bet action={action} />);
    expect(wrapper.find('Styled(Text)').at(2).prop('children')).toEqual('test');
  });
  it('supports onPress prop', () => {
    const onPress = jest.fn();
    const wrapper = shallow(<Bet onPress={onPress} />);
    wrapper.find('Styled(Button)').simulate('press');
    expect(onPress).toBeCalled();
  });
});
