import React from 'react';
import { shallow } from 'enzyme';

import Event from '../Event';


describe('Event test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Event />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('Styled(Text)').prop('children')).toEqual('');
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<Event style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports name prop', () => {
    const name = 'test';
    const wrapper = shallow(<Event name={name} />);
    expect(wrapper.find('Styled(Text)').prop('children')).toEqual(name);
  });
  it('supports markets prop', () => {
    const markets = ['k1', 'k2'];
    const wrapper = shallow(<Event markets={markets} />);
    expect(wrapper.find('Market')).toHaveLength(2);
    expect(wrapper.find('Market').at(0).prop('id')).toEqual('k1');
  });
  it('supports MarketComponent prop', () => {
    const MockComponent = () => 'test';
    const wrapper = shallow(<Event markets={['s']} MarketComponent={MockComponent} />);
    expect(wrapper.find('MockComponent')).toHaveLength(1);
  });
});
