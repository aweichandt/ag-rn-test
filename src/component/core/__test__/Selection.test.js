import React from 'react';
import { shallow } from 'enzyme';

import Selection from '../Selection';

describe('Selection test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Selection />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('primary')).toBeTruthy();
    expect(wrapper.prop('success')).toBeFalsy();
    expect(wrapper.prop('bordered')).toBeTruthy();
    expect(wrapper.find('Styled(Text)').at(0).prop('children')).toEqual('');
    expect(wrapper.find('Styled(Text)').at(1).prop('children')).toEqual(0);
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<Selection style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports name prop', () => {
    const name = 'test';
    const wrapper = shallow(<Selection name={name} />);
    expect(wrapper.find('Styled(Text)').at(0).prop('children')).toEqual(name);
  });
  it('supports price prop', () => {
    const price = 22.31;
    const wrapper = shallow(<Selection price={price} />);
    expect(wrapper.find('Styled(Text)').at(1).prop('children')).toEqual(price);
  });
  it('supports onPress prop', () => {
    const onPress = jest.fn();
    const wrapper = shallow(<Selection onPress={onPress} />);
    wrapper.simulate('press');
    expect(onPress).toBeCalledWith(false);
  });
});

describe('Selection selected test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Selection selected />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('primary')).toBeFalsy();
    expect(wrapper.prop('success')).toBeTruthy();
    expect(wrapper.prop('bordered')).toBeFalsy();
    expect(wrapper.find('Styled(Text)').at(0).prop('children')).toEqual('');
    expect(wrapper.find('Styled(Text)').at(1).prop('children')).toEqual(0);
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<Selection selected style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports onPress prop', () => {
    const onPress = jest.fn();
    const wrapper = shallow(<Selection selected onPress={onPress} />);
    wrapper.simulate('press');
    expect(onPress).toBeCalledWith(true);
  });
});
