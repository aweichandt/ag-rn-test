import React from 'react';
import { shallow } from 'enzyme';

import NoEvents from '../NoEvents';

describe('NoEvents test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<NoEvents />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('Styled(Button)')).toHaveLength(0);
  });
  it('renders as expected with press action', () => {
    const onPress = jest.fn();
    const wrapper = shallow(
      <NoEvents onPress={onPress} />,
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.find('Styled(Button)').simulate('press');
    expect(onPress).toBeCalled();
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<NoEvents style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
});
