import React from 'react';
import { shallow } from 'enzyme';

import Market from '../Market';

describe('Market test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<Market />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('Styled(Text)').prop('children')).toEqual('');
  });
  it('supports style prop', () => {
    const style = { flex: 1 };
    const wrapper = shallow(<Market style={style} />);
    expect(wrapper.prop('style')).toEqual(style);
  });
  it('supports name prop', () => {
    const name = 'test';
    const wrapper = shallow(<Market name={name} />);
    expect(wrapper.find('Styled(Text)').prop('children')).toEqual(name);
  });
  it('supports selections prop', () => {
    const selections = ['k1', 'k2'];
    const wrapper = shallow(<Market selections={selections} />);
    expect(wrapper.find('Selection')).toHaveLength(2);
    expect(wrapper.find('Selection').at(0).prop('id')).toEqual('k1');
  });
  it('supports MarketComponent prop', () => {
    const MockComponent = () => 'test';
    const wrapper = shallow(<Market selections={['s']} SelectionComponent={MockComponent} />);
    expect(wrapper.find('MockComponent')).toHaveLength(1);
  });
});
