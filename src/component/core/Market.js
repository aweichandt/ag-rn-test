import React from 'react';
import {
  View,
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
import { CardItem, Text, Body } from 'native-base';
import PropTypes from 'prop-types';
import Selection from './Selection';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
  },
  title: {
    fontSize: 16,
    alignSelf: 'flex-start',
  },
  selections: {
    padding: 10,
    alignSelf: 'stretch',
    flexShrink: 1,
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
});

const Market = ({
  style, id, name, selections, SelectionComponent,
}) => (
  <CardItem bordered key={id} style={style}>
    <Body>
      <Text style={styles.title}>{name}</Text>
      <View style={styles.selections}>
        {selections.map(k => (
          <SelectionComponent id={k} key={k} />
        ))}
      </View>
    </Body>
  </CardItem>
);
Market.propTypes = {
  style: ViewPropTypes.style,
  id: PropTypes.string,
  name: PropTypes.string,
  selections: PropTypes.arrayOf(PropTypes.string),
  SelectionComponent: PropTypes.func,
};
Market.defaultProps = {
  style: styles.container,
  id: undefined,
  name: '',
  selections: [],
  SelectionComponent: Selection,
};

export default Market;
