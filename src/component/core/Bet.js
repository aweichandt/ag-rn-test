import React from 'react';
import {
  View,
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
import { Text, Button } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  title: {
    fontSize: 12,
    paddingTop: 10,
  },
  price: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
  },
  button: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLabel: {
    fontSize: 10,
  },
});

const Bet = ({
  style, id, name, price, onPress, action,
}) => (
  <View key={id} style={style}>
    <Text style={styles.title}>{name}</Text>
    <Text style={style.price}>{price}</Text>
    <Button
      light
      small
      style={styles.button}
      onPress={onPress}
    >
      <Text style={styles.buttonLabel}>{action}</Text>
    </Button>
  </View>
);
Bet.propTypes = {
  style: ViewPropTypes.style,
  id: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  action: PropTypes.string,
  onPress: PropTypes.func,
};
Bet.defaultProps = {
  style: styles.container,
  id: undefined,
  name: '',
  price: 0,
  action: 'Delete',
  onPress: undefined,
};

export default Bet;
