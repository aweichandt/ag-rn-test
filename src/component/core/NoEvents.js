import React from 'react';
import {
  View,
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import { Text, Body, Button } from 'native-base';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 14,
  },
  button: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonLabel: {
    fontSize: 14,
  },
});

const NoEvents = ({ style, onPress }) => (
  <View style={style}>
    <Body>
      <Text style={styles.text}>
        No events found yet..
      </Text>
      {onPress ? (
        <Button
          transparent
          primary
          small
          style={styles.button}
          onPress={onPress}
        >
          <Text style={styles.buttonLabel}>retry</Text>
        </Button>
      ) : undefined}
    </Body>
  </View>
);
NoEvents.propTypes = {
  style: ViewPropTypes.style,
  onPress: PropTypes.func,
};
NoEvents.defaultProps = {
  style: styles.container,
  onPress: undefined,
};

export default NoEvents;
