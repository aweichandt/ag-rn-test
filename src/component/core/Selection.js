import React from 'react';
import {
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
import { Button, Text } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    margin: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 12,
  },
  price: {
    fontSize: 14,
  },
});

class Selection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    const { onPress, selected } = this.props;
    if (onPress) {
      onPress(selected);
    }
  }

  render() {
    const {
      style, id, name, price, selected,
    } = this.props;
    return (
      <Button
        primary={!selected}
        success={selected}
        bordered={!selected}
        key={id}
        style={style}
        onPress={this.onPress}
      >
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.price}>{price}</Text>
      </Button>
    );
  }
}
Selection.propTypes = {
  style: ViewPropTypes.style,
  id: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  selected: PropTypes.bool,
  onPress: PropTypes.func,
};
Selection.defaultProps = {
  style: styles.container,
  id: undefined,
  name: '',
  price: 0,
  selected: false,
  onPress: undefined,
};

export default Selection;
