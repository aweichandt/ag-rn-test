import React from 'react';
import {
  View,
  StyleSheet,
  ViewPropTypes,
} from 'react-native';
// import PropTypes from 'prop-types';
import { Text } from 'native-base';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
  },
});

const NoBet = ({ style }) => (
  <View style={style}>
    <Text style={styles.text}>you have not made any bet</Text>
  </View>
);
NoBet.propTypes = {
  style: ViewPropTypes.style,
};
NoBet.defaultProps = {
  style: styles.container,
};

export default NoBet;
