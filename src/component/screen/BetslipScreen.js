import React from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
} from 'react-native';
import { Container } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
  },
  safe: {
    flex: 1,
  },
});

const BetslipScreen = ({ children, ...props }) => (
  <SafeAreaView style={styles.safe}>
    <Container style={styles.container}>
      {children}
      <FlatList {...props} />
    </Container>
  </SafeAreaView>
);
BetslipScreen.propTypes = {
  children: PropTypes.node,
};
BetslipScreen.defaultProps = {
  children: undefined,
};

export default BetslipScreen;
