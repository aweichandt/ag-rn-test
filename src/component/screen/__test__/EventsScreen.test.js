import React from 'react';
import { shallow } from 'enzyme';

import EventsScreen from '../EventsScreen';

describe('EventsScreen test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<EventsScreen />);
    expect(wrapper).toMatchSnapshot();
  });
  it('supports onMount', () => {
    const mock = jest.fn();
    const wrapper = shallow(<EventsScreen onMount={mock} />);
    wrapper.simulate('mount');
    expect(mock).toBeCalled();
  });
  it('supports children', () => {
    const wrapper = shallow(<EventsScreen>test</EventsScreen>);
    expect(wrapper.find('Styled(Container)').prop('children')).toContainEqual('test');
  });
});
