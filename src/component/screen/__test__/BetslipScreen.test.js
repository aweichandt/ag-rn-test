import React from 'react';
import { shallow } from 'enzyme';

import BetslipScreen from '../BetslipScreen';

describe('BetslipScreen test suite', () => {
  it('renders as expected', () => {
    const wrapper = shallow(<BetslipScreen />);
    expect(wrapper).toMatchSnapshot();
  });
  it('supports children', () => {
    const wrapper = shallow(<BetslipScreen>test</BetslipScreen>);
    expect(wrapper.find('Styled(Container)').prop('children')).toContainEqual('test');
  });
});
