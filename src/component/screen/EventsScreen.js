import React from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
} from 'react-native';
import { Container } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
  },
  safe: {
    flex: 1,
  },
});
class EventsScreen extends React.PureComponent {
  componentDidMount() {
    const { onMount } = this.props;
    onMount();
  }

  render() {
    const { children, onMount, ...props } = this.props;
    return (
      <SafeAreaView style={styles.safe}>
        <Container style={styles.container}>
          <FlatList {...props} />
          {children}
        </Container>
      </SafeAreaView>
    );
  }
}
EventsScreen.propTypes = {
  onMount: PropTypes.func,
  children: PropTypes.node,
};
EventsScreen.defaultProps = {
  onMount: () => {},
  children: undefined,
};

export default EventsScreen;
