import React from 'react';
import { Provider } from 'react-redux';

import store from './model/store';
import AppNavigator from './container/navigation/AppNavigator';

const App = () => (
  <Provider store={store}>
    <AppNavigator />
  </Provider>
);

export default App;
