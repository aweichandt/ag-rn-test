import { connect } from 'react-redux';
import {
  openDrawerAction,
  closeDrawerAction,
} from '../../model/action';
import { DrawerButton } from '../../component/common';

const mapStateToProps = (state, { close }) => ({
  icon: close ? 'close' : 'menu',
});
const mapDispatchToProps = (dispatch, { close }) => ({
  onPress: () => dispatch(close ? closeDrawerAction() : openDrawerAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerButton);
