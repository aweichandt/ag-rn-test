import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import navigation from '../../../model/middleware/navigation';

import DrawerNavigationButton from '../DrawerNavigationButton';

const middlewares = [navigation]; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  navigation: {
    index: 0,
    isDrawerOpen: false,
    isTransitioning: false,
    openId: 0,
    closeId: 0,
    toggleId: 0,
    routes: [
      { key: 'EVENTS', routeName: 'EVENTS' },
    ],
  },
};

describe('DrawerNavigationButton test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <DrawerNavigationButton />,
      { context: { store } },
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('icon')).toEqual('menu');
    wrapper.simulate('press');
    expect(store.getActions()).toEqual([
      { type: 'Navigation/OPEN_DRAWER' },
    ]);
  });
  it('renders as expected with close behavior', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <DrawerNavigationButton close />,
      { context: { store } },
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('icon')).toEqual('close');
    wrapper.simulate('press');
    expect(store.getActions()).toEqual([
      { type: 'Navigation/CLOSE_DRAWER' },
    ]);
  });
});
