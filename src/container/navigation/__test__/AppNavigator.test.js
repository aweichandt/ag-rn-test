import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import navigation from '../../../model/middleware/navigation';

import AppNavigator from '../AppNavigator';

const middlewares = [navigation]; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  navigation: {
    index: 0,
    isDrawerOpen: false,
    isTransitioning: false,
    openId: 0,
    closeId: 0,
    toggleId: 0,
    routes: [
      { key: 'EVENTS', routeName: 'EVENTS' },
    ],
  },
};

describe('AppNavigator test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <AppNavigator />,
      { context: { store } },
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('state')).toEqual(initialState.navigation);
  });
});
