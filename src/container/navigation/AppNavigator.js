import { connect } from 'react-redux';
import AppRouter from '../../navigation/router';
import { createNavigator } from '../../navigation/helpers/redux';

const mapStateToProps = ({ navigation }) => ({
  state: navigation,
});
const AppNavigator = createNavigator(AppRouter);
export default connect(mapStateToProps)(AppNavigator);
