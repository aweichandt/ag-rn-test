import { connect } from 'react-redux';
import { Selection } from '../../component/core';
import { betAction, unbetAction } from '../../model/action';


const mapStateToProps = ({ selections, betslip }, { id }) => ({
  ...selections[id],
  selected: betslip.includes(id),
});
const mapDispatchToProps = (dispatch, { id }) => ({
  onPress: selected => dispatch(selected ? unbetAction(id) : betAction(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Selection);
