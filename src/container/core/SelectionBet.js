import { connect } from 'react-redux';
import { Bet } from '../../component/core';
import { unbetAction } from '../../model/action';

const mapStateToProps = ({ selections }, { id }) => ({
  ...selections[id],
});
const mapDispatchToProps = (dispatch, { id }) => ({
  onPress: () => dispatch(unbetAction(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Bet);
