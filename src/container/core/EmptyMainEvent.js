import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NoEvents } from '../../component/core';
import { fetchEventsAction } from '../../model/action';
import { EVENTS } from '../../model/const/networkEvents';

const mapStateToProps = ({ network }) => ({
  fetching: network[EVENTS] || false,
});
const mapDispatchToProps = dispatch => ({
  onPress: () => dispatch(fetchEventsAction()),
});

const EmptyMainEvent = ({ onPress, fetching }) => (
  <NoEvents onPress={fetching ? undefined : onPress} />
);
EmptyMainEvent.propTypes = {
  onPress: PropTypes.func.isRequired,
  fetching: PropTypes.bool.isRequired,
};


export default connect(mapStateToProps, mapDispatchToProps)(EmptyMainEvent);
