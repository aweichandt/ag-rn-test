import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import EventMarket from '../EventMarket';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  markets: {
    mkt0: {
      id: 'mkt0',
      name: 'market0',
      selections: ['s0', 's1'],
    },
    mkt1: {
      id: 'mkt1',
      name: 'market1',
      selections: ['s3'],
    },
  },
};

describe('EventMarket test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const market = initialState.markets.mkt0;
    const wrapper = shallow(
      <EventMarket id="mkt0" />,
      { context: { store } },
    ).dive();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('name')).toEqual(market.name);
    expect(wrapper.prop('selections')).toEqual(market.selections);
    expect(wrapper.prop('SelectionComponent')).not.toBeUndefined();
  });
  it('supports no market', () => {
    const store = mockStore(initialState);
    shallow(
      <EventMarket />,
      { context: { store } },
    );
  });
});
