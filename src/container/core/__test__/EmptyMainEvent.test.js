import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import EmptyMainEvent from '../EmptyMainEvent';
import { EVENTS } from '../../../model/const/networkEvents';

jest.mock('../../../model/action', () => {
  const actions = require.requireActual();
  return { ...actions, fetchEventsAction: () => ({ type: 'test' }) };
});

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  network: {},
};

describe('EmptyMainEvent test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <EmptyMainEvent />,
      { context: { store } },
    ).dive();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('onPress')).not.toBeUndefined();
    wrapper.dive().find('Styled(Button)').simulate('press');
    expect(store.getActions()).toEqual([
      { type: 'test' },
    ]);
  });
  it('hides retry when fetching', () => {
    const store = mockStore({ ...initialState, network: { [EVENTS]: true } });
    const wrapper = shallow(
      <EmptyMainEvent />,
      { context: { store } },
    ).dive();
    expect(wrapper.prop('onPress')).toBeUndefined();
  });
});
