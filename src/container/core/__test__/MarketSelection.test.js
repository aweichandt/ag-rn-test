import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import MarketSelection from '../MarketSelection';
import { BET, UNBET } from '../../../model/const/actionNames';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  selections: {
    s0: { id: 's0', name: 'sel0', price: 1.22 },
    s1: { id: 's1', name: 'sel1', price: 2.11 },
  },
  betslip: [],
};

describe('MarketSelection test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const selection = initialState.selections.s0;
    const wrapper = shallow(
      <MarketSelection id="s0" />,
      { context: { store } },
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('name')).toEqual(selection.name);
    expect(wrapper.prop('price')).toEqual(selection.price);
    expect(wrapper.prop('selected')).toBeFalsy();
    wrapper.dive().simulate('press');
    expect(store.getActions()).toEqual([
      { type: BET, value: 's0' },
    ]);
  });
  it('renders selected as expected', () => {
    const store = mockStore({
      ...initialState,
      betslip: ['s0'],
    });
    const selection = initialState.selections.s0;
    const wrapper = shallow(
      <MarketSelection id="s0" />,
      { context: { store } },
    );
    expect(wrapper.prop('name')).toEqual(selection.name);
    expect(wrapper.prop('price')).toEqual(selection.price);
    expect(wrapper.prop('selected')).toBeTruthy();
    wrapper.dive().simulate('press');
    expect(store.getActions()).toEqual([
      { type: UNBET, value: 's0' },
    ]);
  });
  it('supports no selection', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <MarketSelection />,
      { context: { store } },
    );
    wrapper.dive().simulate('press');
    expect(store.getActions()).toEqual([
      { type: BET, value: undefined },
    ]);
  });
});
