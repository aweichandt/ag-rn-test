import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import MainEvent from '../MainEvent';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  events: {
    evt0: {
      id: 'evt0',
      name: 'event0',
      markets: ['mkt0', 'mkt1'],
    },
    evt1: {
      id: 'evt1',
      name: 'event1',
      markets: [],
    },
  },
};

describe('MainEvent test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const event = initialState.events.evt0;
    const wrapper = shallow(
      <MainEvent id="evt0" />,
      { context: { store } },
    ).dive();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('name')).toEqual(event.name);
    expect(wrapper.prop('markets')).toEqual(event.markets);
    expect(wrapper.prop('MarketComponent')).not.toBeUndefined();
  });
  it('renders as expected for empty event', () => {
    const store = mockStore(initialState);
    const event = initialState.events.evt1;
    const wrapper = shallow(
      <MainEvent id="evt1" />,
      { context: { store } },
    ).dive();
    expect(wrapper.prop('name')).toEqual(event.name);
    expect(wrapper.prop('markets')).toEqual(event.markets);
  });
  it('supports no event', () => {
    const store = mockStore(initialState);
    shallow(
      <MainEvent />,
      { context: { store } },
    );
  });
});
