import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import SelectionBet from '../SelectionBet';
import { UNBET } from '../../../model/const/actionNames';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  selections: {
    s0: { id: 's0', name: 'sel0', price: 1.22 },
    s1: { id: 's1', name: 'sel1', price: 2.11 },
  },
};

describe('SelectionBet test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const selection = initialState.selections.s0;
    const wrapper = shallow(
      <SelectionBet id="s0" />,
      { context: { store } },
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('name')).toEqual(selection.name);
    expect(wrapper.prop('price')).toEqual(selection.price);
    wrapper.dive().find('Styled(Button)').simulate('press');
    expect(store.getActions()).toEqual([
      { type: UNBET, value: 's0' },
    ]);
  });
  it('supports no bet', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <SelectionBet />,
      { context: { store } },
    );
    wrapper.dive().find('Styled(Button)').simulate('press');
    expect(store.getActions()).toEqual([
      { type: UNBET, value: undefined },
    ]);
  });
});
