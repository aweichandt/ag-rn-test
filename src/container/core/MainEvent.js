import React from 'react';
import { connect } from 'react-redux';
import { Event } from '../../component/core';
import EventMarket from './EventMarket';

const mapStateToProps = ({ events }, { id }) => ({
  ...events[id],
});

const MainEvent = props => (
  <Event {...props} MarketComponent={EventMarket} />
);

export default connect(mapStateToProps)(MainEvent);
