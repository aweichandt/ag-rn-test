import React from 'react';
import { connect } from 'react-redux';
import { Market } from '../../component/core';
import MarketSelection from './MarketSelection';

const mapStateToProps = ({ markets }, { id }) => ({
  ...markets[id],
});

const EventMarket = props => (
  <Market {...props} SelectionComponent={MarketSelection} />
);

export default connect(mapStateToProps)(EventMarket);
