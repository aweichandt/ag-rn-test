import React from 'react';
import { connect } from 'react-redux';
import { EventsScreen } from '../../component/screen';
import { MainEvent, EmptyMainEvent } from '../core';
import { fetchEventsAction } from '../../model/action';
import { EVENTS } from '../../model/const/networkEvents';

const keyExtractor = key => key;
/* eslint-disable react/prop-types */
const renderItem = ({ item }) => (
  <MainEvent id={item} />
);
/* eslint-enable react/prop-types */

const mapStateToProps = ({ events, network }) => ({
  data: Object.keys(events),
  refreshing: network[EVENTS] || false,
});
const mapDispatchToProps = dispatch => ({
  onMount: () => dispatch(fetchEventsAction()),
  onRefresh: () => dispatch(fetchEventsAction()),
});

const MainEventsScreen = props => (
  <EventsScreen
    {...props}
    keyExtractor={keyExtractor}
    renderItem={renderItem}
    ListEmptyComponent={EmptyMainEvent}
  />
);

export default connect(mapStateToProps, mapDispatchToProps)(MainEventsScreen);
