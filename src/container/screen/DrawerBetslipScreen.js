import React from 'react';
import { connect } from 'react-redux';
import { BetslipScreen } from '../../component/screen';
import { SelectionBet } from '../core';
import { Separator } from '../../component/common';
import { NoBet } from '../../component/core';
import DrawerNavigationButton from '../navigation/DrawerNavigationButton';

const keyExtractor = key => key;
/* eslint-disable react/prop-types */
const renderItem = ({ item }) => (
  <SelectionBet id={item} />
);
/* eslint-enable react/prop-types */

const mapStateToProps = ({ betslip: data }) => ({
  data,
});

const DrawerBetslipScreen = props => (
  <BetslipScreen
    {...props}
    keyExtractor={keyExtractor}
    renderItem={renderItem}
    ItemSeparatorComponent={Separator}
    ListEmptyComponent={NoBet}
  >
    <DrawerNavigationButton close />
  </BetslipScreen>
);

export default connect(mapStateToProps)(DrawerBetslipScreen);
