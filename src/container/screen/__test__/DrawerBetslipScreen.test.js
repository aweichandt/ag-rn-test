import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import DrawerBetslipScreen from '../DrawerBetslipScreen';

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  betslip: ['s1', 's2'],
};

describe('DrawerBetslipScreen test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <DrawerBetslipScreen />,
      { context: { store } },
    ).dive();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('data')).toEqual(initialState.betslip);
    expect(wrapper.prop('renderItem')).not.toBeUndefined();
    expect(wrapper.prop('keyExtractor')).not.toBeUndefined();
    expect(wrapper.prop('ListEmptyComponent')).not.toBeUndefined();
  });
});
