import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import MainEventsScreen from '../MainEventsScreen';
import { EVENTS } from '../../../model/const/networkEvents';

jest.mock('../../../model/action', () => {
  const actions = require.requireActual();
  return { ...actions, fetchEventsAction: () => ({ type: 'test' }) };
});

const middlewares = []; // you can mock any middlewares here if necessary
const mockStore = configureStore(middlewares);
const initialState = {
  events: {
    evt0: {
      id: 'evt0',
      name: 'event0',
      markets: ['mkt0', 'mkt1'],
    },
    evt1: {
      id: 'evt1',
      name: 'event1',
      markets: [],
    },
  },
  network: {},
};

describe('MainEventsScreen test suite', () => {
  it('renders as expected', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <MainEventsScreen />,
      { context: { store } },
    ).dive();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.prop('data')).toEqual(['evt0', 'evt1']);
    expect(wrapper.prop('refreshing')).toBeFalsy();
    expect(wrapper.prop('renderItem')).not.toBeUndefined();
    expect(wrapper.prop('keyExtractor')).not.toBeUndefined();
  });
  it('renders as expected when refreshing', () => {
    const store = mockStore({ ...initialState, network: { [EVENTS]: true } });
    const wrapper = shallow(
      <MainEventsScreen />,
      { context: { store } },
    ).dive();
    expect(wrapper.prop('data')).toEqual(['evt0', 'evt1']);
    expect(wrapper.prop('refreshing')).toBeTruthy();
    expect(wrapper.prop('renderItem')).not.toBeUndefined();
    expect(wrapper.prop('keyExtractor')).not.toBeUndefined();
    expect(wrapper.prop('ListEmptyComponent')).not.toBeUndefined();
  });
  it('supports onMount', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <MainEventsScreen />,
      { context: { store } },
    ).dive();
    wrapper.dive().simulate('mount');
    expect(store.getActions()).toEqual([
      { type: 'test' },
    ]);
  });
  it('supports onRefresh', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <MainEventsScreen />,
      { context: { store } },
    ).dive();
    wrapper.dive().simulate('refresh');
    expect(store.getActions()).toEqual([
      { type: 'test' },
    ]);
  });
});
