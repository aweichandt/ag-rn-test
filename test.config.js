import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import fetch from 'jest-fetch-mock';

// Enyzme config
configure({ adapter: new Adapter() });

// Whatwg-fetch mock
global.fetch = fetch;
global.navigator = {
  geolocation: {
    getCurrentPosition: () => Promise.resolve()
  }
};